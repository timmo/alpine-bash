ARG BUILD_FROM=alpine:3.9.2
# hadolint ignore=DL3006
FROM ${BUILD_FROM}

# Environment variables
ENV \
    HOME="/root" \
    LANG="C.UTF-8" \
    PS1="$(whoami)@$(hostname):$(pwd)$ "

# Build arch argument
ARG BUILD_ARCH=amd64

# Set shell
SHELL ["/bin/ash", "-o", "pipefail", "-c"]

# Install system
# hadolint ignore=DL3003
RUN \
    set -o pipefail \
    \
    && echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/main' >> /etc/apk/repositories \
    && echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/community' >> /etc/apk/repositories \
    && echo '@edge http://dl-cdn.alpinelinux.org/alpine/edge/testing' >> /etc/apk/repositories \
    \
    && apk add --no-cache \
        bash=4.4.19-r1 \
        nodejs-current=11.3.0-r0 \
    \
    && rm -fr /tmp/*

# Set shell
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

RUN \
    set -o pipefail

CMD ["/bin/bash"]

# Build arguments
ARG BUILD_DATE
ARG BUILD_REF
ARG BUILD_VERSION

# Labels
LABEL \
    maintainer="Timmo <contact@timmo.xyz>" \
    org.label-schema.description="Alpine Linux with Bash (Azure Pipelines support)" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.name="Alpine Bash" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://timmo.dev" \
    org.label-schema.usage="https://gitlab.com/timmo/alpine-bash/tree/master/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/timmo/alpine-bash" \
    org.label-schema.vendor="Timmo"